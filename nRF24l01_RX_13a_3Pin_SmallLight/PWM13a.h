
#define Attiny13a_ActivatePWMClock TCCR0A |= _BV(WGM01)|_BV(WGM00)
#define Attiny13a_PWMOutOnPin0 DDRB |= _BV(PB0); TCCR0A |= _BV(COM0A1)
#define Attiny13a_PWMOutOnPin1 DDRB |= _BV(PB1); TCCR0A |= _BV(COM1A1)
#define Attiny13a_NO_PWMOutOnPin0 DDRB &= ~_BV(PB0); TCCR0A &= ~_BV(COM0A1)
#define Attiny13a_NO_PWMOutOnPin1 DDRB &= ~_BV(PB1); TCCR0A &= ~_BV(COM1A1)
#define Attiny13a_ResetPWMTimer TCCR0A = 0
#define Attiny13a_PWM8BitValuePin0(x) OCR0A = x
#define Attiny13a_PWM8BitValuePin1(x) OCR1A = x
#define Attiny13a_PWM_SetPrescaler(x) TCCR0B = (TCCR0B & ~((1<<CS02)|(1<<CS01)|(1<<CS00))) | x

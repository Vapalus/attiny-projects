#ifndef SPITRICKS_h
#define SPITRICKS_h

#include "NRF24L01.h"

//#define PIN_REDUCE
#define NO_ENABLE

class SPITricks{
  public:
    
    static void initSPITricks(){
      pinMode(CEq, OUTPUT);
      pinMode(CSNq, OUTPUT);
      pinMode(SCKq, OUTPUT);
      pinMode(MOSIq, OUTPUT);
      pinMode(MISOq, INPUT_PULLUP);
    }
    
    static void setPinHalfPower(int iPin){
      pinMode(iPin, INPUT_PULLUP);
    }
    
    static void setPinFullPower(int iPin){
      pinMode(iPin, OUTPUT);
      digitalWrite(iPin, 1);
    }
    
    static void setPinLowPower(int iPin){
      digitalWrite(iPin, 0);
      pinMode(iPin, OUTPUT);
    }
    
    static void spiEnable(){
#ifndef PIN_REDUCE
      digitalWrite(CSNq, 0);
#else
      setPinHalfPower(CSNq);
#endif
    }
    
    static void spiDisable(){
#ifndef PIN_REDUCE
      digitalWrite(CSNq, 1);
#else
      setPinLowPower(CSNq);
#endif
    }
    
    static void clockHIGH(){
#ifndef PIN_REDUCE
      digitalWrite(SCKq, 1);
#else
      setPinFullPower(CSNq);
#endif
    }
    
    static void clockLOW(){
#ifndef PIN_REDUCE
      digitalWrite(SCKq, 0);
#else
      setPinHalfPower(CSNq);
#endif
    }
    
    static void mosiHIGH(){
      digitalWrite(MOSIq, 1);
    }
    
    static void mosiLOW(){
      digitalWrite(MOSIq, 0);
    }
    
    static int misoValue(){
      return digitalRead(MISOq);
    }
    
    static void chipEnable(){
#ifndef NO_ENABLE
      digitalWrite(CEq, 1);
#endif
    }
    
    static void chipDisable(){
#ifndef NO_ENABLE
      digitalWrite(CEq, 0);
#endif
    }
};
//*********************************************
#endif

#ifndef NRF24L01_h
#define NRF24L01_h

#include "API.H"

//---------------------------------------------
#define TX_ADR_WIDTH    5   
// 5 unsigned chars TX(RX) address width
#define TX_PLOAD_WIDTH  1  
// 20 unsigned chars TX payload
//---------------------------------------------

#define CEq       7
#define CSNq      6
#define SCKq      5
#define MOSIq     4
#define MISOq     3
#define IRQq      2

//*********************************************
#endif

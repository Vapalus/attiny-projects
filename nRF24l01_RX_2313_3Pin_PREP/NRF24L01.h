#ifndef NRF24L01_h
#define NRF24L01_h

#include "API.H"

//---------------------------------------------
#define TX_ADR_WIDTH    5   
// 5 unsigned chars TX(RX) address width
#define TX_PLOAD_WIDTH  1  
// 20 unsigned chars TX payload
//---------------------------------------------
#define CEq       7
//8
#define CSNq      6
//9
#define SCKq      5
//10
#define MOSIq     4
//11
#define MISOq     3
//12
#define IRQq      2
//13
//*********************************************
#endif

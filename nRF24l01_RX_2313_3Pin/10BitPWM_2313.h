
#define Attiny2313_ActivatePWMClock_Pin12_Pin13_1 TCCR1B = (1 << CS10)
#define Attiny2313_ActivatePWMClock_Pin11_1 TCCR0B = (1 << CS10)
#define Attiny2313_PWMOutOnPin11 DDRB|=(1<<PB2)
#define Attiny2313_PWMOutOnPin12 DDRB|=(1<<PB3)
#define Attiny2313_PWMOutOnPin13 DDRB|=(1<<PB4)
#define Attiny2313_ResetPWMTimer_Pin12_Pin13 TCCR1A = 0
#define Attiny2313_ResetPWMTimer_Pin11 TCCR0A = 0
#define Attiny2313_PWMTimerOnPin11 TCCR0A |= (1 << COM0A1) | (1 << WGM00)
#define Attiny2313_PWMTimerOnPin12 TCCR1A |= (1 << COM1A1) | (1 << WGM11) | (1 << WGM10)
#define Attiny2313_PWMTimerOnPin13 TCCR1A |= (1 << COM1B1) | (1 << WGM11) | (1 << WGM10)
#define Attiny2313_PWM10BitValuePin12(x) OCR1A = x
#define Attiny2313_PWM10BitValuePin13(x) OCR1B = x
#define Attiny2313_PWM8BitValuePin11(x) OCR0A = x
#define Attiny2313_PWM8BitValuePin12(x) OCR1A = x << 2
#define Attiny2313_PWM8BitValuePin13(x) OCR1B = x << 2

#define Attiny2313_initPWM_Pin11 Attiny2313_PWMOutOnPin11; Attiny2313_ResetPWMTimer_Pin11; Attiny2313_PWMTimerOnPin11; Attiny2313_ActivatePWMClock_Pin11_1
#define Attiny2313_initPWM_Pin12 Attiny2313_PWMOutOnPin12; Attiny2313_ResetPWMTimer_Pin12_Pin13; Attiny2313_PWMTimerOnPin12; Attiny2313_ActivatePWMClock_Pin12_Pin13_1
#define Attiny2313_initPWM_Pin13 Attiny2313_PWMOutOnPin13; Attiny2313_ResetPWMTimer_Pin12_Pin13; Attiny2313_PWMTimerOnPin13; Attiny2313_ActivatePWMClock_Pin12_Pin13_1
#define Attiny2313_initPWM_Pin12_Pin13 Attiny2313_PWMOutOnPin12; Attiny2313_PWMOutOnPin13; Attiny2313_ResetPWMTimer_Pin12_Pin13; Attiny2313_PWMTimerOnPin12; Attiny2313_PWMTimerOnPin13; Attiny2313_ActivatePWMClock_Pin12_Pin13_1
#define Attiny2313_initPWM_Pin_11_Pin12_Pin13 Attiny2313_initPWM_Pin12_Pin13; Attiny2313_initPWM_Pin11

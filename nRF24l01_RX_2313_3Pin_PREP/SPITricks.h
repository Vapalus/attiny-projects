#ifndef SPITRICKS_h
#define SPITRICKS_h

#include "NRF24L01.h"

class SPITricks{
  public:
    
    static void initSPITricks(){
      pinMode(CEq, OUTPUT);
      pinMode(CSNq, OUTPUT);
      pinMode(SCKq, OUTPUT);
      pinMode(MOSIq, OUTPUT);
      pinMode(MISOq, INPUT_PULLUP);
    }
    
    static void setPinHalfPower(int iPin){
      digitalWrite(iPin, 0);
      pinMode(iPin, INPUT_PULLUP);
    }
    
    static void setPinFullPower(int iPin){
      pinMode(iPin, OUTPUT);
      digitalWrite(iPin, 1);
    }
    
    static void setPinLowPower(int iPin){
      pinMode(iPin, OUTPUT);
      digitalWrite(iPin, 0);
    }
    
    static void spiEnable(){
      digitalWrite(CSNq, 0);
      //setPinHalfPower(CSNq);
    }
    
    static void spiDisable(){
      digitalWrite(CSNq, 1);
      //setPinLowPower(CSNq);
    }
    
    static void clockHIGH(){
      digitalWrite(SCKq, 1);
      //setPinFullPower(SCKq);
    }
    
    static void clockLOW(){
      digitalWrite(SCKq, 0);
      //setPinHalfPower(SCKq);
    }
    
    static void mosiHIGH(){
      digitalWrite(MOSIq, 1);
    }
    
    static void mosiLOW(){
      digitalWrite(MOSIq, 0);
    }
    
    static int misoValue(){
      return digitalRead(MISOq);
    }
    
    static void chipEnable(){
      digitalWrite(CEq, 1);
    }
    
    static void chipDisable(){
      digitalWrite(CEq, 0);
    }
};
//*********************************************
#endif

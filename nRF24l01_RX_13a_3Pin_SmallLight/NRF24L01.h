#ifndef NRF24L01_h
#define NRF24L01_h

#include "API.H"

//---------------------------------------------
#define TX_ADR_WIDTH    5   
// 5 unsigned chars TX(RX) address width
#define TX_PLOAD_WIDTH  1  
// 20 unsigned chars TX payload
//---------------------------------------------
/*
#define CEq       1
#define CSNq      4
#define SCKq      3
#define MOSIq     2
#define MISOq     1
#define IRQq      1
*/
#define CEq       1
#define CSNq      3
#define SCKq      2
#define MOSIq     1
#define MISOq     1
#define IRQq      1
// 2 SCK
// 1 MISO or MIMO
// 4 MOSI
// 3 CSN
//*********************************************
#endif

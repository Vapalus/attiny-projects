/*********************************************************************
**  Device:  nRF24L01+                                              **
**  File:   EF_nRF24L01_TX.c                                        **
**                                                                  **
**                                                                  **
**  Copyright (C) 2011 ElecFraks.                                   **
**  This example code is in the public domain.                      **
**                                                                  **
**  Description:                                                    **
**  This file is a sample code for your reference.                  **
**  It's the v1.1 nRF24L01+ by arduino                              **
**  Created by ElecFreaks. Robi.W,24 July 2011                      **
**                                                                  **
**  http://www.elecfreaks.com                                       **
**                                                                  **
**   SPI-compatible                                                 **
**   CSq - to digital pin 8                                          **
**   CSNq - to digital pin 9  (SS pin)                               **
**   SCKq - to digital pin 10 (SCK pin)                              **
**   MOSIq - to digital pin 11 (MOSI pin)                            **
**   MISOq - to digital pin 12 (MISO pin)                            **
**   IRQq - to digital pin 13 (MISO pin)                             **
*********************************************************************/

#include "NRF24L01.h"
#include "PWM13a.h"
#include "SPITricks.h"
#include "light_ws2812.h"


//***************************************************
#define TX_ADR_WIDTH    5   // 5 unsigned chars TX(RX) address width
#define TX_PLOAD_WIDTH  32  // 32 unsigned chars TX payload


unsigned char TX_ADDRESS[TX_ADR_WIDTH]  = 
{
  "2Node" //-1 == port; Small Light = ID 1, ergo 2
}; // Define a static TX address

unsigned char rx_buf[TX_PLOAD_WIDTH];
unsigned char tx_buf[TX_PLOAD_WIDTH];










//***************************************************



struct pixel {
	uint8_t w;
	uint8_t b;
	uint8_t r;
  uint8_t g;
};
struct pixel current = {0,0,0,0};
struct pixel toShow = {0,0,0,0};

void setColor(pixel input){
  toShow = input;
}

void updateColor(){
  int difR = toShow.r - current.r;
  int difG = toShow.g - current.g;
  int difB = toShow.b - current.b;
  int difW = toShow.w - current.w;
  difR = difR > 0 ? 1 : difR == 0 ? 0 : -1;
  difG = difG > 0 ? 1 : difG == 0 ? 0 : -1;
  difB = difB > 0 ? 1 : difB == 0 ? 0 : -1;
  difW = difW > 0 ? 1 : difW == 0 ? 0 : -1;
  current.r = current.r + difR;
  current.g = current.g + difG;
  current.b = current.b + difB;
  current.w = current.w + difW;
  ws2812_setleds_rgbw((struct cRGBW *)&current, 10);
}




//***************************************************








void setup() 
{
  digitalWrite(0,0);
  pinMode(0, OUTPUT);

  delay(1);
  int iCounter = 0;
  for(iCounter = 255; iCounter--;)
  {
    toShow.r = iCounter;
    toShow.g = iCounter;
    toShow.b = iCounter;
    toShow.w = iCounter; 

    ws2812_setleds_rgbw((struct cRGBW *)&toShow, 10);
    delay(1);
  }
  
  
  
  pinMode(CEq,  OUTPUT);
  pinMode(SCKq, OUTPUT);
  pinMode(CSNq, OUTPUT);
  pinMode(MOSIq,  OUTPUT);
  pinMode(MISOq, INPUT);
  
  SPI_RW_Reg(FLUSH_RX,0); 
  
  init_io();
  unsigned char status=SPI_Read(STATUS);
  RX_Mode();
  
}
void loop() 
{
  for(;;)
  {
    unsigned char red = 0;
    unsigned char green = 0;
    unsigned char blue = 0;
    unsigned char status = SPI_Read(STATUS);                         // read register STATUS's value
    if(status&RX_DR)                                                 // if receive data ready (TX_DS) interrupt
    {
      SPI_Read_Buf(RD_RX_PLOAD, rx_buf, TX_PLOAD_WIDTH);             // read playload to rx_buf
      SPI_RW_Reg(FLUSH_RX,0);                                        // clear RX_FIFO
      
      toShow.r = rx_buf[0];
      toShow.g = rx_buf[1];
      toShow.b = rx_buf[2];
      toShow.w = rx_buf[0] & rx_buf[1]; 

      //ws2812_setleds_rgbw((struct cRGBW *)&toShow, 10);
      delay(1);
    }
    SPI_RW_Reg(WRITE_REG+STATUS,status);                             // clear RX_DR or TX_DS or MAX_RT interrupt flag
    delay(1);
    updateColor();
    delay(1);
  }
}

//**************************************************
// Function: init_io();
// Description:
// flash led one time,chip enable(ready to TX or RX Mode),
// Spi disable,Spi clock line init high
//**************************************************
void init_io(void)
{
  //digitalWrite(IRQq, 0);
  SPITricks::chipDisable();      // chip enable
  SPITricks::spiDisable();                 // Spi disable 
}

/**************************************************
 * Function: SPI_RW();
 * 
 * Description:
 * Writes one unsigned char to nRF24L01, and return the unsigned char read
 * from nRF24L01 during write, according to SPI protocol
 **************************************************/
unsigned char SPI_RW(unsigned char Byte)
{
  unsigned char i;
  for(i=0;i<8;i++)                      // output 8-bit
  {
    if(Byte&0x80)
    {
      SPITricks::mosiHIGH();    // output 'unsigned char', MSB to MOSI
    }
    else
    {
      SPITricks::mosiLOW();
    }
    SPITricks::clockHIGH();                      // Set SCK high..
    Byte <<= 1;                         // shift next bit into MSB..
    if(SPITricks::misoValue() == 1)
    {
      Byte |= 1;                        // capture current MISO bit
    }
    SPITricks::clockLOW();          // ..then set SCK low again
  }
  return(Byte);                     // return read unsigned char
}
/**************************************************/

/**************************************************
 * Function: SPI_RW_Reg();
 * 
 * Description:
 * Writes value 'value' to register 'reg'
/**************************************************/
unsigned char SPI_RW_Reg(unsigned char reg, unsigned char value)
{
  unsigned char status;

  SPITricks::spiEnable();                   // CSN low, init SPI transaction
  status = SPI_RW(reg);                   // select register
  SPI_RW(value);                          // ..and write value to it..
  SPITricks::spiDisable();                   // CSN high again

  return(status);                   // return nRF24L01 status unsigned char
}
/**************************************************/

/**************************************************
 * Function: SPI_Read();
 * 
 * Description:
 * Read one unsigned char from nRF24L01 register, 'reg'
/**************************************************/
unsigned char SPI_Read(unsigned char reg)
{
  unsigned char reg_val;

  SPITricks::spiEnable();           // CSN low, initialize SPI communication...
  SPI_RW(reg);                   // Select register to read from..
  reg_val = SPI_RW(0);           // ..then read register value
  SPITricks::spiDisable();          // CSN high, terminate SPI communication

  return(reg_val);               // return register value
}
/**************************************************/

/**************************************************
 * Function: SPI_Read_Buf();
 * 
 * Description:
 * Reads 'unsigned chars' #of unsigned chars from register 'reg'
 * Typically used to read RX payload, Rx/Tx address
/**************************************************/
unsigned char SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char bytes)
{
  unsigned char status,i;

  SPITricks::spiEnable();                  // Set CSN low, init SPI tranaction
  status = SPI_RW(reg);             // Select register to write to and read status unsigned char

  for(i=0;i<bytes;i++)
  {
    pBuf[i] = SPI_RW(0);    // Perform SPI_RW to read unsigned char from nRF24L01
  }

  SPITricks::spiDisable();                   // Set CSN high again

  return(status);                  // return nRF24L01 status unsigned char
}
/**************************************************/

/**************************************************
 * Function: SPI_Write_Buf();
 * 
 * Description:
 * Writes contents of buffer '*pBuf' to nRF24L01
 * Typically used to write TX payload, Rx/Tx address
/**************************************************/
unsigned char SPI_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned char bytes)
{
  unsigned char status,i;

  SPITricks::spiEnable();                   // Set CSN low, init SPI tranaction
  status = SPI_RW(reg);             // Select register to write to and read status unsigned char
  for(i=0;i<bytes; i++)             // then write all unsigned char in buffer(*pBuf)
  {
    SPI_RW(*pBuf++);
  }
  SPITricks::spiDisable();                  // Set CSN high again
  return(status);                  // return nRF24L01 status unsigned char
}
/**************************************************/

/**************************************************
 * Function: RX_Mode();
 * 
 * Description:
 * This function initializes one nRF24L01 device to
 * RX Mode, set RX address, writes RX payload width,
 * select RF channel, datarate & LNA HCURR.
 * After init, CE is toggled high, which means that
 * this device is now ready to receive a datapacket.
/**************************************************/
void RX_Mode(void)
{
  SPITricks::chipDisable();
  SPI_Write_Buf(WRITE_REG + RX_ADDR_P0, TX_ADDRESS, TX_ADR_WIDTH); // Use the same address on the RX device as the TX device
  SPI_RW_Reg(WRITE_REG + EN_AA, 0x01);      // Enable Auto.Ack:Pipe0
  SPI_RW_Reg(WRITE_REG + EN_RXADDR, 0x01);  // Enable Pipe0
  SPI_RW_Reg(WRITE_REG + RF_CH, 0x4c);        // Select RF channel 40
  SPI_RW_Reg(WRITE_REG + RX_PW_P0, TX_PLOAD_WIDTH); // Select same RX payload width as TX Payload width
  SPI_RW_Reg(WRITE_REG + RF_SETUP, 0x27);   // TX_PWR:0dBm, Datarate:2Mbps, LNA:HCURR
  SPI_RW_Reg(WRITE_REG + CONFIG, 0x0f);     // Set PWR_UP bit, enable CRC(2 unsigned chars) & Prim:RX. RX_DR enabled..
  SPITricks::chipEnable();                             // Set CE pin high to enable RX device
  //  This device is now ready to receive one packet of 16 unsigned chars payload from a TX device sending to address
  //  '3443101001', with auto acknowledgment, retransmit count of 10, RF channel 40 and datarate = 2Mbps.
}
/**************************************************/

#ifndef SPITRICKS_h
#define SPITRICKS_h

#include "NRF24L01.h"

//#define PIN_REDUCE_CSN
//#define PIN_REDUCE_CLOCK
#define NO_ENABLE
#define WAIT_TIME 128
#define USE_MIMO

class SPITricks{
  private:

  public:

#ifdef USE_MIMO
    static int _MOSIVAL;
#endif
    
    static void initSPITricks(){
      pinMode(CEq, OUTPUT);
      pinMode(CSNq, OUTPUT);
      pinMode(SCKq, OUTPUT);
      pinMode(MOSIq, OUTPUT);
      pinMode(MISOq, INPUT_PULLUP);
    }
    
    static void setPinHalfPower(int iPin){
      pinMode(iPin, INPUT_PULLUP);
    }
    
    static void setPinFullPower(int iPin){
      pinMode(iPin, OUTPUT);
    }
    
    static void setPinLowPower(int iPin){
      digitalWrite(iPin, 0);
      pinMode(iPin, OUTPUT);
    }
    
    static void spiEnable(){
#ifndef PIN_REDUCE_CSN
      digitalWrite(CSNq, 0);
#else
      setPinHalfPower(CSNq);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static void spiDisable(){
#ifndef PIN_REDUCE_CSN
      digitalWrite(CSNq, 1);
#else
      setPinLowPower(CSNq);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static void clockHIGH(){
#ifndef PIN_REDUCE_CLOCK
      digitalWrite(SCKq, 1);
#else
      setPinFullPower(SCKq);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static void clockLOW(){
#ifndef PIN_REDUCE_CLOCK
      digitalWrite(SCKq, 0);
#else
      setPinHalfPower(SCKq);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static void mosiHIGH(){
      digitalWrite(MOSIq, 1);
#ifdef USE_MIMO
      _MOSIVAL = 1;
#endif      
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static void mosiLOW(){
      digitalWrite(MOSIq, 0);
#ifdef USE_MIMO
      _MOSIVAL = 0;
#endif      
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static int misoValue(){
#ifdef USE_MIMO
      pinMode(MISOq, INPUT);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
      int value = digitalRead(MISOq);
#ifdef USE_MIMO
      pinMode(MISOq, OUTPUT);
      digitalWrite(MOSIq, _MOSIVAL);
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
#endif
      return value;
    }
    
    static void chipEnable(){
#ifndef NO_ENABLE
      digitalWrite(CEq, 1);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
    
    static void chipDisable(){
#ifndef NO_ENABLE
      digitalWrite(CEq, 0);
#endif
#ifdef WAIT_TIME
      delayMicroseconds(WAIT_TIME);
#endif
    }
};

#ifdef USE_MIMO
int SPITricks::_MOSIVAL = 0;
#endif
//*********************************************
#endif

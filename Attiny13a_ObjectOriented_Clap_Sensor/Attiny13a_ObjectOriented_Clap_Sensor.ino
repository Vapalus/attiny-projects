#include <avr/io.h>
#include <avr/interrupt.h>

#define LED_PIN_GREEN 1
#define LED_PIN_BLUE 0
#define INPUT_PIN 3
#define CLAP_SPIKE 3
#define CLAP_ERROR_TOLERANCE 20

byte noise = 0;
byte lastTimeNoise = 0;
byte iCurrentADCVal = 0;
byte secondsSinceLastClap = 0;
//byte currentPos = 255;
//byte value = 0;

//TIMING_LENGTH is a necessity atm due to my fzcking poor coding skills
#define TIMING_LENGTH_ONE 6
const byte tune_one[TIMING_LENGTH_ONE] = {
                  40,//400 ms between first and second clap
                  20, //200 between second and third
                  20, //and so on
                  40,
                  70,
                  40
                  };
//Jingle bells clap:
#define TIMING_LENGTH_TWO 10
byte tune_two[TIMING_LENGTH_TWO] = {
                  30, //jing
                  30, //le
                  70, //bomb
                  30, //jing
                  30, //le
                  70, //bomb
                  30, //jing
                  30, //le
                  50, //all
                  20  //my ass
                  };

class ClappingTune{
  public:
  byte pos;
  byte pin;
  byte* tune;
  byte tuneLength;
  byte toggleValue;
  //////////////////////////////////////////////
  ClappingTune(byte pinToUse, byte* tuneToUse, byte tuneLengthToUse){
    pin = pinToUse;
    tune = tuneToUse;
    tuneLength = tuneLengthToUse;
    pos = 255;
    toggleValue = 0;
  }
  //////////////////////////////////////////////
  void onClap(byte secondsSinceLastClap){
    if (pos == 255){
      pos = 0;
    }
    else{
      if (abs(secondsSinceLastClap - tune[pos])< CLAP_ERROR_TOLERANCE){ //If you aren't smart enough for clapping in tact, have some error tolerance
        digitalWrite(pin,!toggleValue);
        pos++;//Increases "currentPos" by one. This is a useful comment. Please read it.
        if (pos == tuneLength){
          //It seems we are done. That means whatever you want to do when the clap-code is correct, you can do it here. I prefer to just set "value", take a shit and call it a day.
          toggleValue ^= 1;
          //Reset current pos, since it's a bad idea to read unknown Memory
          pos = 255;
        }
      }
      else{
        //Learn to clap, bro. Failure.
        pos = 255;
      }
    }
  }
  void afterClapCheck(){
    digitalWrite(pin,toggleValue);
  }
};

ClappingTune firstTune(LED_PIN_GREEN, tune_one, TIMING_LENGTH_ONE);
ClappingTune secondTune(LED_PIN_BLUE, tune_two, TIMING_LENGTH_TWO);




byte ignore = 0;

//Timer which is being called every 0.03 seconds. Please set your attiny to 9.3 MHz, else this thing won't work right.
ISR(TIM0_COMPA_vect)
{
  ignore ^= 1;
  if(ignore){
    return;
  }
  //Just prevents a "double detection"
  if (noise && !lastTimeNoise){
    //digitalWrite(LED_PIN_GREEN,!value); //Not really necessairy, just shows it detected a "clap" which makes it a more user-friendly piece of trash
    firstTune.onClap(secondsSinceLastClap);
    secondTune.onClap(secondsSinceLastClap);

    secondsSinceLastClap = 0;
    lastTimeNoise = 0;
  }
  else{
    //This is just here because I'm lazy. Makes the "clap" detection possible and also sets the value - at the same time. I'm a genius.
    firstTune.afterClapCheck();
    secondTune.afterClapCheck();
  }
  lastTimeNoise = 0;
  if (noise){
    noise = 0;
    lastTimeNoise = 1; //setting "lastTimeNoise" prevents any double-detection of the same clap, since a clap normally takes less than 0.03 seconds.
  }
  
  secondsSinceLastClap +=6; //add 0.06 seconds
  if (secondsSinceLastClap > 250){
    //Prevent integer overflow. Byte overflow. Whatever.
    secondsSinceLastClap = 250;
    //Reset the clapping position to prevent hilariarse situations.
    firstTune.pos = 255;
    secondTune.pos = 255;
  }
}

void adc_workaround_attiny13a (void)
{
    // Set the prescaler to clock/128 & enable ADC
    // Necessairy since it won't activate in "int main(void)" without using the "setup" makro/functionality/function
    ADCSRA |= (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);
}

int main(void)
{
  adc_workaround_attiny13a();

  //This doesn't work right with using "setup()" so I guess I'll just ignore any good coding practice and write this code slamming my head on the keyboard
  //I am using a timer to get the timing right. Who would have thought that using a timer helps with timing.
  TCCR0A |= _BV(WGM01); // set timer counter mode to CTC
  TCCR0B |= _BV(CS02)|_BV(CS00); // set prescaler to 1024 (CLK=9200000Hz/1024/256=35.1Hz, 0.03s)
  OCR0A = 255; // set Timer's counter max value
  TIMSK0 |= _BV(OCIE0A); // enable Timer CTC interrupt
  sei(); // enable global interrupts
  
  pinMode(LED_PIN_GREEN, OUTPUT); //Here comes the lazy
  pinMode(LED_PIN_BLUE, OUTPUT); //Here comes the lazy
  pinMode(INPUT_PIN, INPUT);  //Why should I use registers to set a fzcking pin, you fools

  while(1){
    //Getting the claps directly from the microphone
    byte iPreviousValue = iCurrentADCVal;
    iCurrentADCVal = analogRead(INPUT_PIN);
    if ((iPreviousValue-iCurrentADCVal)>CLAP_SPIKE){ //Guess what "Clap Spike" means, loser
      noise = 1;
    }
  };
}
